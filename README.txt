
##-----------------------------------------------------------------##
##				USAGE				   ##			
##-----------------------------------------------------------------##	

Author: 	Philipp Saggau
Date:		03.08.2021
Affilation:	Departement of Geography, Christian-Albrechts University Kiel

## -------------------------- Execution -------------------------- ##

The data refers to the following article: 

Saggau, P., M. Kuhwald, W. B. Hamer, R. Duttmann (2021): Are compacted tramlines underestimated features in soil erosion modelling? 
A catchment‐scale analysis using a process‐based soil erosion model. In: Land Degradtaion & Development. doi: 10.1002/ldr.4161

The data only contains distributable raw data and R-codes.

The scripts should be executed successively (01 - 13) in the following order:

01_RAW_INPUT
02_RAW_COVAR
03_HYDRO_CATCH
04_REG_TEXTURE
05_REG_SOC
06_INPUT_DPROC
07_COVER_E3D
08_INPUT_E3D
09_UPDATE_SOIL_DATA
10_RAIN_EVENT
11_CALIBRATION
12_OUTPUT_E3D
13_TESTDATA_E3D

## -------------------------- Colophon -------------------------- ##

Version of used Software and Packages

R version 4.0.4 (2021-02-15)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 18363)

Matrix products: default

locale:
[1] LC_COLLATE=English_United States.1252  LC_CTYPE=English_United States.1252    LC_MONETARY=English_United States.1252 LC_NUMERIC=C                          
[5] LC_TIME=English_United States.1252    

attached base packages:
[1] splines   grid      stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] randomForest_4.6-14 gam_1.20            foreach_1.5.1       hydroGOF_0.4-0      zoo_1.8-9           car_3.0-10          carData_3.0-4       RSAGA_1.3.0        
 [9] plyr_1.8.6          shapefiles_0.7      foreign_0.8-81      magrittr_2.0.1      moments_0.14        Metrics_0.1.4       gstat_2.0-6         automap_1.0-14     
[17] corrplot_0.84       compositions_2.0-1  soiltexture_1.5.1   geoR_1.8-1          rdwd_1.4.0          lubridate_1.7.10    reshape2_1.4.4      gridExtra_2.3      
[25] ggforce_0.3.3       ggpubr_0.4.0        ggplot2_3.3.3       rgeos_0.5-5         fasterize_1.0.3     tidyr_1.1.3         dplyr_1.0.5         sf_0.9-7           
[33] raster_3.4-5        sp_1.4-5           

loaded via a namespace (and not attached):
 [1] RandomFieldsUtils_0.5.3 xts_0.12.1              tensorA_0.36.2          tools_4.0.4             backports_1.2.1         utf8_1.1.4              R6_2.5.0               
 [8] KernSmooth_2.23-18      DBI_1.1.1               colorspace_2.0-0        withr_2.4.1             hydroTSM_0.6-0          tidyselect_1.1.0        splancs_2.01-40        
[15] bayesm_3.1-4            curl_4.3                compiler_4.0.4          RandomFields_3.3.8      scales_1.1.1            DEoptimR_1.0-8          classInt_0.4-3         
[22] robustbase_0.93-7       pbapply_1.4-3           stringr_1.4.0           rio_0.5.26              pkgconfig_2.0.3         rlang_0.4.10            readxl_1.3.1           
[29] FNN_1.1.3               farver_2.1.0            generics_0.1.0          berryFunctions_1.19.1   zip_2.1.1               Rcpp_1.0.6              munsell_0.5.0          
[36] fansi_0.4.2             abind_1.4-5             lifecycle_1.0.0         stringi_1.5.3           MASS_7.3-53             maptools_1.1-1          parallel_4.0.4         
[43] forcats_0.5.1           crayon_1.4.1            lattice_0.20-41         haven_2.3.1             hms_1.0.0               pillar_1.5.1            tcltk_4.0.4            
[50] spacetime_1.2-4         ggsignif_0.6.1          codetools_0.2-18        glue_1.4.2              data.table_1.14.0       vctrs_0.3.6             tweenr_1.0.2           
[57] cellranger_1.1.0        gtable_0.3.0            purrr_0.3.4             polyclip_1.10-0         reshape_0.8.8           openxlsx_4.2.3          broom_0.7.5            
[64] e1071_1.7-4             rstatix_0.7.0           class_7.3-18            tibble_3.1.0            intervals_0.15.2        iterators_1.0.13        units_0.7-0            
[71] ellipsis_0.3.1     