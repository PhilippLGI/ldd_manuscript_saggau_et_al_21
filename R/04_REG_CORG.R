
###################################################################################
## ----------------------------------------------------------------------------- ##
## Interpolation Eingangsdaten Modellierung ---- Corg
## ----------------------------------------------------------------------------- ##
###################################################################################



## ----------------------------------------------------------------------------- ##
## Pakete ----
## ----------------------------------------------------------------------------- ##
if(!("corrplot" %in% rownames(installed.packages()))){install.packages("corrplot")}
if(!("ggplot2" %in% rownames(installed.packages()))){install.packages("ggplot2")}
if(!("moments" %in% rownames(installed.packages()))){install.packages("moments")}
if(!("raster" %in% rownames(installed.packages()))){install.packages("raster")}
if(!("car" %in% rownames(installed.packages()))){install.packages("car")}
if(!("gstat" %in% rownames(installed.packages()))){install.packages("gstat")}
if(!("hydroGOF" %in% rownames(installed.packages()))){install.packages("hydroGOF")}
if(!("automap" %in% rownames(installed.packages()))){install.packages("automap")}
if(!("dplyr" %in% rownames(installed.packages()))){install.packages("dplyr")}
if(!("gam" %in% rownames(installed.packages()))){install.packages("gam")}
if(!("randomForest" %in% rownames(installed.packages()))){install.packages("randomForest")}
if(!("magrittr" %in% rownames(installed.packages()))){install.packages("magrittr")}
if(!("tidyr" %in% rownames(installed.packages()))){install.packages("tidyr")}
if(!("sf" %in% rownames(installed.packages()))){install.packages("sf")}


library(corrplot)
library(ggplot2)
library(moments)
library(raster)
library(car)
library(gstat)
library(hydroGOF)
library(automap)
library(dplyr)
library(gam)
library(randomForest)
library(magrittr)
library(tidyr)
library(sf)

## ----------------------------------------------------------------------------- ##
## Statistical preparation of dataset ---- Corg
## ----------------------------------------------------------------------------- ##

# Set WD and creating Table containung coordinates, Covariables -- Corg
if(rev(strsplit(getwd(), "/")[[1]])[1]!= "e3d_tramlines_paper"){
  setwd("../")
  if(rev(strsplit(getwd(), "/")[[1]])[1]!= "e3d_tramlines_paper"){
    alldir <- list.dirs()
    setwd(alldir[which(unlist(Map(function(i){rev(strsplit(alldir[i], "/")[[1]])[1]}, i = 1:length(alldir))) %in% "e3d_tramlines_paper")])
  }
}

#read CORG data
Corg_Pts <- read.csv("./analysis/data/derived_data/CORG.csv")


# Regard the distribution of our sample for identifiying outliers
ggplot(Corg_Pts, aes(y = CORG)) +
  geom_boxplot(fill = "grey", outlier.colour = "black", outlier.shape = "*", outlier.size = 8) +
  theme_bw() +
  labs(title = "CORG %", x = "N = 305", y = "Ct %")
ggsave("./analysis/figures/05_Outlier.png", width = 3, height = 5, dpi = 300)

## Test for need of transformation depending on skewness W-Value and P-Value of the distribution
# If W-Value > 0.9 and P-Value > 0.05 --> No Transformation (Normal Distribution)
# Else If Skewness >1 --> Log()
# Else Sqrt()
#Corg = ifelse(shapiro.test(Corg_Pts$CORG)$statistic > 0.9 and  shapiro.test(Corg_Pts$CORG)$p-value > 0.05,
#              Corg_Pts$Corg,Corg_Pts$Corg),ifelse(skewness(Corg_Pts$CORG)> 1, Corg_Pts$Corg,log(Corg_Pts$Corg)),sqrt(Corg_CoVar$Corg))

ST <- shapiro.test(Corg_Pts$CORG)
SK <- skewness(Corg_Pts$CORG)
KT <- kurtosis(Corg_Pts$CORG)


n <- length(Corg_Pts$CORG)
hk <- round((3.49 * sd(Corg_Pts$CORG) * n^(-1/3)), digits = 1); hk

png(file='./analysis/figures/05_DensHist.png',width = 1000, height = 800,res=120)
hd.trans <- hist(Corg_Pts$CORG , freq = F, col = "grey", xlim = c(0,15), ylim = c(0,0.8),
                 breaks = seq(0,15,1), xlab = "CORG % (*10 log.)",
                 ylab = "probability density",
                 main = "Density Histogram, C - transformiert, M?hlenbach-S?d")
curve(dnorm(x, mean(Corg_Pts$CORG), sd(Corg_Pts$CORG)), add = T, lwd = 2, lty = 2)
text(5, 0.6, paste0("W = ",round(ST$statistic,3),"\nP = ",round(ST$p.value,6),"
                  \nSkewness = ",round(SK,3),"\nKurtosis = ",round(KT,3),""))
dev.off()

# Transformation necessary, because no normal distribution can be found according to (p- and W-vlaue)
# log() seems reasonable for transformation, as skewness is greater 1
# Multiplication of Corg with 10 because otherwise log would turn into some negative values

Corg_Trans <- log(Corg_Pts$CORG*10)
ST <- shapiro.test(Corg_Trans)
SK <- skewness(Corg_Trans)
KT <- kurtosis(Corg_Trans)

## Looking at new distribution
n <- length(Corg_Trans)
hk <- round((3.49 * sd(Corg_Trans) * n^(-1/3)), digits = 1)

png(file='./analysis/figures/05_DensHistLog.png',width = 1000, height = 800,res=120)
hd.trans <- hist(Corg_Trans , freq = F, col = "grey", xlim = c(0.5,6), ylim = c(0,1.2),
                 breaks = seq(0,30,hk), xlab = "log(Corg [%]*10) ",
                 ylab = "probability density",
                 main = "Density Histogram, log(Corg*10)")
curve(dnorm(x, mean(Corg_Trans), sd(Corg_Trans)), add = T, lwd = 2, lty = 2)
text(5, 0.8, paste0("W = ",round(ST$statistic,2),"\nP = ",round(ST$p.value,5),"
                  \nSkewness = ",round(SK,2),"\nKurtosis = ",round(KT,2),""))
dev.off()

## Looking at outlier
Corg_Pts$Corg_Trans <- Corg_Trans

ggplot(Corg_Pts, aes(x = , y = Corg_Trans)) +
  geom_boxplot(fill = "grey", outlier.colour = "black", outlier.shape = "*", outlier.size = 8) +
  theme_bw() +
  labs(title = "log(Corg*10)", x = "N = 305", y = "log(Corg [%] *10)")
ggsave("./analysis/figures/05_OutlierLog.png", width = 3, height = 5, dpi = 300)


###################################################################################
## ----------------------------------------------------------------------------- ##
## Extracting Model for with Main Covariables ----
## ----------------------------------------------------------------------------- ##
###################################################################################


## ----------------------------------------------------------------------------- ##
## correlation-matrix ----
## ----------------------------------------------------------------------------- ##

#Loading Covariables
Covar_All <- read.csv("./analysis/data/derived_data/MB_COVAR_NL.csv")
Corg_Covar_DF <- merge(Corg_Pts,Covar_All, by = "SID")

# Creating Heatplot for overviwe of correlating variables
corMat <- cor(Corg_Covar_DF[,5:ncol(Corg_Covar_DF)])

png(file='./analysis/figures/05_corrplot.png',width = 1400, height = 1400,res=170)
corrplot.mixed(corMat, number.cex=0.8, tl.cex = 0.9, tl.col = "black",
               outline=FALSE, mar=c(0,0,2,2), upper="square", bg=NA)
dev.off()


# Creating the best Model from all covariables through step-wise regression
# We exclude DEM because of low correlation with Corg but high multicolinearity with channel_Base (R = 0.91)
# We also exclude LS-Factor because of its high multicolinearity with SLOPE (R = 0.92), but its lower correlation with
# Same was done for RSP with muticolinearity of 0.7 and 0.78 with TPI and Channel Distance.

Mod_All <- CORG ~ STWI + WETNESS  + SLOPE + CHNL_BASE + CHNL_DIST + CONVE +
  HCURV + SHADE +  TPI + TRI + VALL_DEPTH + VCURV

Mod_All <- lm(Mod_All, data = Corg_Covar_DF)

Mod_Best <- step(Mod_All,direction="backward")

Mod_Best_Term <- Mod_Best$terms
#CORG ~ WETNESS + CHNL_BASE + CHNL_DIST + TPI + TRI + VALL_DEPTH
#AIC 501,13


##  Checking regression-model for Homocedacity, Cooks Distance, Heterocdascitiy, non-linear-Regresion
png(file='./analysis/figures/05_Regressionplot.png',width = 800, height = 800,res=120)
par(mfrow=c(2,2))
plot(Mod_Best,mfrow=c(2,2))
confint(Mod_Best)
dev.off()

outlierTest(Mod_Best)
png(file='./analysis/figures/05_leveragePlots.png',width = 800, height = 800,res=120)
leveragePlots(Mod_Best)
dev.off()
ncvTest(Mod_Best)
png(file='./analysis/figures/05_ceresPlots.png',width = 800, height = 800,res=120)
ceresPlots(Mod_Best)
dev.off()



## ----------------------------------------------------------------------------- ##
## Creating Model ----
## ----------------------------------------------------------------------------- ##

#Loading Needed Variables from correlation analysis
# Stack Variables and Extract by points
Raster.Stack <- stack("./analysis/data/derived_data/derived_geodat/Topo_DEM/WETNESS.sdat", 
                      "./analysis/data/derived_data/derived_geodat/Topo_DEM/TPI.sdat",
                      "./analysis/data/derived_data/derived_geodat/Topo_DEM/TRI.sdat", 
                      "./analysis/data/derived_data/derived_geodat/Topo_DEM/CHNL_DIST.sdat", 
                      "./analysis/data/derived_data/derived_geodat/Topo_DEM/CHNL_BASE.sdat", 
                      "./analysis/data/derived_data/derived_geodat/Topo_DEM/VALL_DEPTH.sdat")

proj4string(Raster.Stack) <- crs("+init=epsg:25832")
rstPixDF <- as(Raster.Stack, "SpatialGridDataFrame")


coordinates(Corg_Pts)<- ~X + Y
proj4string(Corg_Pts)<- crs("+init=epsg:25832")

Corg_CoVarMod <- raster::extract(Raster.Stack, Corg_Pts,sp=TRUE)
CoVarMod_DF  = as.data.frame(Corg_CoVarMod)
write.csv(CoVarMod_DF, "./analysis/data/derived_data/Corg_CoVarMod.csv", row.names = FALSE)


## ----------------------------------------------------------------------------- ##
## Kriging with external Drift (KED) ----
## ----------------------------------------------------------------------------- ##

# kriging with external Drift using modelled covariables
kedKrigMap <- autoKrige(Corg_Trans ~ WETNESS + CHNL_BASE + CHNL_DIST + TPI + TRI + VALL_DEPTH,
                        Corg_CoVarMod, rstPixDF)

png(file='./analysis/figures/05_Int_KED_Corg.png',width = 1400, height = 1400,res=170)
plot(kedKrigMap)
dev.off()


#Validation
loocv_ked <- krige.cv(Corg_Trans ~ WETNESS + CHNL_BASE + CHNL_DIST + TPI + TRI + VALL_DEPTH, Corg_CoVarMod,
                      kedKrigMap$var_model)

# retransformation of Corg for loocv
loocv_ked$var1.pred <- exp(loocv_ked$var1.pred)/10
loocv_ked$observed<- exp(loocv_ked$observed)/10
loocv_ked$residual<- loocv_ked$observed-loocv_ked$var1.pred


# Calculating statistical characteristics
rmse_ked <- rmse(loocv_ked$var1.pred, loocv_ked$observed)

mae_ked <- mae(loocv_ked$observed, loocv_ked$var1.pred)

rsq_ked <- summary(lm(Corg_CoVarMod$Corg_Trans ~ loocv_ked$var1.pred))$r.squared

nsr_ked <- kedKrigMap$var_model$psill[1] / (kedKrigMap$var_model$psill[1] +
                                              kedKrigMap$var_model$psill[2]) * 100

KED_perf <- c(rmse_ked,mae_ked,rsq_ked, nsr_ked)


#Erstellung einer Tabelle mit allen wichtigen Modellkenwerten und Export der Tabelle
Stat_Reg <- data.frame(Verfahren = c("RMSE","MAE","RSQ", "NSR"), 
                     KED = KED_perf)

write.csv2(Stat_Reg,file = "./analysis/data/derived_data/Stats_C_auto.csv")

Perf_comp <- Stat_Reg %>% 
  gather(key=Technique, value=Error, -Verfahren)

ggplot(data=Perf_comp %>% filter(Verfahren != "NSR"),
       aes(x=Verfahren, y=Error, fill=Technique)) +
  geom_bar(stat="identity", position=position_dodge()) +
  scale_fill_brewer(palette="Paired") + theme_minimal() 

ggsave("./analysis/figures/05_Int_compplot.png", width = 7, height = 4, dpi = 300)

## ----------------------------------------------------------------------------- ##
## Selection and Export ----
## ----------------------------------------------------------------------------- ##

#The best model for Corg interpolation is in dependency of statistical values the KED Method
# With lowest RMSE and MAE and highest RSQ


# plotting distribution of residuals
KED_bu <- bubble(loocv_ked, "residual", maxsize = 2, scales = list(draw = T),
                 col = c("gray30", "gray70"), xlab = "E/m", ylab = "N/m",
                 main = "SOC: 1-fold CV residuals", sub = "UTM-Coordinates",
                 key.entries = seq(-5, 5, by = 1),
                 plot.lims = c(floor(min(Corg_CoVarMod$Corg_Trans, loocv_ked$var1.pred)),
                               ceiling(max(Corg_Trans, loocv_ked$var1.pred))));plot(KED_bu)

# plotting modelled vs. observed values
svg(filename="analysis/figures/Corg_Fit.svg")
plot(loocv_ked$observed, loocv_ked$var1.pred,
     xlim=c(0,10),
     ylim=c(0,10),
     pch=19,
     cex=0.5,
     col="Blue",
     xlab="Observed SOC-Content [%]",
     ylab="Predicted SOC-Content [%]",
     main="Observed vs. Predicted SOC-Content\n(0-30 cm)",
     grid(col="lightgrey",lty=2))

abline(0,1,col="red")
abline(lm(var1.pred ~ observed, data=loocv_ked))
text(1.75,8, label=paste0("RMSE = ",round(rmse_ked,2)," \n  MAE = ",round(mae_ked,2),
                          "\n     Rsq = ",round(rsq_ked,2),""))

dev.off()


# Transformation and plotting Resultgrid
residKrigRstLayer <- as(kedKrigMap$krige_output, "RasterLayer")

KED <-as(residKrigRstLayer, "SpatialGridDataFrame")

KED <- raster(KED)

KED <- round(KED, digits = 2)

Catchment <- st_read("./analysis/data/derived_data/derived_geodat/Shapes/Catchment_MB.shp")

# Clipping DEMs to Catchment extent and Export
C_Catch  <- mask(KED ,Catchment)
C_Catch   <- crop(C_Catch  ,Catchment)
NAvalue(C_Catch)<- -9999

plot(C_Catch, main="Corg\nKED",
     xlab="Longitude", ylab="Latitude", cex.main=0.8, cex.axis=0.7, cex=0.8)

writeRaster(C_Catch, "analysis/data/derived_data/derived_geodat/Regio/CORG.asc",
            format = "ascii", overwrite = TRUE)

dev.off()
